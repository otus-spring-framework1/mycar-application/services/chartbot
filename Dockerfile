FROM openjdk:11-jdk-slim
ADD ./jar/chartbot-1.0.jar chat-bot.jar
CMD ["java", "-jar", "chat-bot.jar"]

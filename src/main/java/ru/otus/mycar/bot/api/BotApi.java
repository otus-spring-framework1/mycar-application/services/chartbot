package ru.otus.mycar.bot.api;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import ru.otus.mycar.bot.model.enums.BotApiNames;

public interface BotApi {
    ReplyKeyboard getApi();
    BotApiNames getBotApiName();
}

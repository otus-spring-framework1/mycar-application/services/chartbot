package ru.otus.mycar.bot.api.botApiImpl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import ru.otus.mycar.bot.api.BotApi;
import ru.otus.mycar.bot.builder.ButtonBuilder;
import ru.otus.mycar.bot.model.enums.BotApiNames;
import ru.otus.mycar.bot.model.enums.CallBackMenu;
import ru.otus.mycar.bot.utils.CallBackMenuFactory;

@Component
@RequiredArgsConstructor
public class StatisticApi implements BotApi {
    private final ButtonBuilder buttonBuilder;
    private final CallBackMenuFactory callBackMenuFactory;

    @Override
    public ReplyKeyboard getApi() {
        return buttonBuilder.addCallBackButtons(callBackMenuFactory.getButtons(CallBackMenu.STATISTIC_MENU)
                                                             .getButtonNames());
    }

    @Override
    public BotApiNames getBotApiName() {
        return BotApiNames.STATISTICS;
    }
}

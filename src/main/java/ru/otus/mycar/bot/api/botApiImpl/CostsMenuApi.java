package ru.otus.mycar.bot.api.botApiImpl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import ru.otus.mycar.bot.api.BotApi;
import ru.otus.mycar.bot.builder.ButtonBuilder;
import ru.otus.mycar.bot.model.enums.BotApiNames;
import ru.otus.mycar.bot.model.enums.KeyboardMenuButtons;
import ru.otus.mycar.bot.utils.KeyboardButtonsFactory;

@Component
@RequiredArgsConstructor
public class CostsMenuApi implements BotApi {
    private final ButtonBuilder buttonBuilder;
    private final KeyboardButtonsFactory keyboardButtonsFactory;

    @Override
    public ReplyKeyboard getApi() {
        return buttonBuilder.addKeyboardButtons(keyboardButtonsFactory.getButtons(KeyboardMenuButtons.COSTS_MENU)
                                                                      .getButtonNames());
    }

    @Override
    public BotApiNames getBotApiName() {
        return BotApiNames.MY_COSTS_API;
    }
}

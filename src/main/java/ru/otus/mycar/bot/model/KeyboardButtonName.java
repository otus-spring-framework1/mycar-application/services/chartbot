package ru.otus.mycar.bot.model;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.otus.mycar.bot.model.enums.BotState;

@Data
@Accessors(chain = true)
public class KeyboardButtonName {
    private String name;
    private BotState botState;
}

package ru.otus.mycar.bot.model.enums;

public enum BotApiNames {
    MAIN_MENU_API, MY_CAR_API, TRANSMISSION_API,
    MY_COSTS_API, STATISTICS, SETTINGS_MENU_API, LANGUAGE_API, MAINTENANCE_API;
}

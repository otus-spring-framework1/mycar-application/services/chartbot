package ru.otus.mycar.bot.model;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.otus.mycar.bot.model.enums.MetricType;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class Metrics {
    private String userId;
    private MetricType metricType;
    private String value;
    private LocalDate date;
}

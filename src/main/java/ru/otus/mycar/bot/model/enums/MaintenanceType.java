package ru.otus.mycar.bot.model.enums;

public enum MaintenanceType {
    TOTAL, REMIND, SCHEDULE
}

package ru.otus.mycar.bot.model.enums;

public enum KeyboardMenuButtons {
    CAR_MENU, COSTS_MENU, MAIN_MENU, SETTINGS_MENU, MAINTENANCE_MENU;
}

package ru.otus.mycar.bot.model.enums;

public enum CallBackMenu {
    TRANSMISSION_MENU, LANGUAGE_MENU, MAINTENANCE_MENU, STATISTIC_MENU
}

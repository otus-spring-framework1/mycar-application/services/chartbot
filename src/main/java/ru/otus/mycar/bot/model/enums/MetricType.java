package ru.otus.mycar.bot.model.enums;

public enum MetricType {
    MILEAGE, GAS, SPARES;
}

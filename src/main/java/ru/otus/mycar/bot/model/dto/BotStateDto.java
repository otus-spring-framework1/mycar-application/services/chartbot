package ru.otus.mycar.bot.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.otus.mycar.bot.model.enums.BotState;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class BotStateDto {
    private String id;
    private BotState botState;
}

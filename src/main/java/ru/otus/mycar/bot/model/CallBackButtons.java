package ru.otus.mycar.bot.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CallBackButtons {
    private String callBackKey;
    private String buttonName;
}

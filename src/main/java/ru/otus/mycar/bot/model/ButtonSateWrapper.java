package ru.otus.mycar.bot.model;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.otus.mycar.bot.model.enums.BotState;

import java.util.Map;

@Data
@Accessors(chain = true)
public class ButtonSateWrapper {
    private BotState botState;
    private Map<String, String> buttonNames;
}

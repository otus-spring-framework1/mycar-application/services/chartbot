package ru.otus.mycar.bot.model;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.otus.mycar.bot.model.enums.MaintenanceType;

@Data
@Accessors(chain = true)
public class CarMaintenance {
    private String userId;
    private int value;
    private MaintenanceType maintenanceType;

}

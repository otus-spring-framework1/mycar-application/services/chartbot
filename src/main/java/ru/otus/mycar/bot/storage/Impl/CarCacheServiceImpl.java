package ru.otus.mycar.bot.storage.Impl;

import org.springframework.stereotype.Service;
import ru.otus.mycar.bot.model.Car;
import ru.otus.mycar.bot.storage.CarCacheService;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class CarCacheServiceImpl implements CarCacheService {
    private final ConcurrentMap<Long, Car> carCache = new ConcurrentHashMap<>();

    @Override
    public void saveCarInCache(long userId, Car car) {
        carCache.put(userId, car);
    }

    @Override
    public void setCarMileage(long userId, String mileage){
        carCache.computeIfPresent(userId, (k, v) -> v.setMileage(Integer.parseInt(mileage)));
    }

    @Override
    public void setCarEngine(long userId, String engine){
        carCache.computeIfPresent(userId, (k, v) -> v.setEngineVolume(Float.parseFloat(engine)));
    }

    @Override
    public void setCarYear(long userId, String year){
        carCache.computeIfPresent(userId, (k, v) -> v.setYear(Integer.parseInt(year)));
    }

    @Override
    public void setCarTransmission(long userId, String transmission){
        carCache.computeIfPresent(userId, (k, v) -> v.setTransmission(transmission));
    }

    @Override
    public void setCarModel(long userId, String model){
        carCache.computeIfPresent(userId, (k, v) -> v.setModel(model));
    }

    @Override
    public Car getCarByUserId(Long userId){
        return carCache.get(userId);
    }
}

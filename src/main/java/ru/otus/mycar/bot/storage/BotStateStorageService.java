package ru.otus.mycar.bot.storage;

import ru.otus.mycar.bot.model.dto.BotStateDto;
import ru.otus.mycar.bot.model.enums.BotState;

public interface BotStateStorageService {
    BotStateDto getUsersCurrentBotState(long userId);
    void setCurrentBotState(long userId, BotState botState);
}


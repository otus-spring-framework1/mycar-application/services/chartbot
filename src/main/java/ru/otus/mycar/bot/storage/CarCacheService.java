package ru.otus.mycar.bot.storage;

import ru.otus.mycar.bot.model.Car;

public interface CarCacheService {
    void saveCarInCache(long userId, Car car);
    void setCarMileage(long userId, String mileage);
    void setCarEngine(long userId, String engine);
    void setCarYear(long userId, String year);
    void setCarTransmission(long userId, String transmission);
    void setCarModel(long userId, String model);
    Car getCarByUserId(Long userId);
}

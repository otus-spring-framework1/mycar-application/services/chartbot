package ru.otus.mycar.bot.storage.Impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.otus.mycar.bot.builder.BotStateBuilder;
import ru.otus.mycar.bot.model.dto.BotStateDto;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.request.BotStateRequestService;
import ru.otus.mycar.bot.storage.BotStateStorageService;

import java.util.Optional;

@Service
@AllArgsConstructor
public class BotStateCarStorageService implements BotStateStorageService {
    private final BotStateRequestService requestService;
    private final BotStateBuilder builder;

    public void setCurrentBotState(long userId, BotState botState) {
        requestService.sendBotStateToCarHandler(builder.getBotStateWrapper(userId,botState));
    }

    public BotStateDto getUsersCurrentBotState(long userId)  {
        return Optional.ofNullable(requestService.getBotStateFromCarHandler(userId))
                       .orElse(builder.getBotStateWrapper(userId,BotState.INFORM));
    }
}

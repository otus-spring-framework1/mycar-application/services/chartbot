package ru.otus.mycar.bot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.api.BotApi;
import ru.otus.mycar.bot.model.enums.BotApiNames;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BotApiFactory {
    private final List<BotApi> botApis;

    public BotApi getBotApi(BotApiNames botApiName) {
        return botApis.stream()
                      .collect(Collectors.toMap(BotApi::getBotApiName, Function.identity()))
                      .get(botApiName);
    }
}

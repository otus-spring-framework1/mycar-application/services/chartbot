package ru.otus.mycar.bot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.settingsMenuBuilder.SettingMenuBuilder;
import ru.otus.mycar.bot.model.enums.BotState;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class SettingMenuFactory {
    private final List<SettingMenuBuilder> settingMenuBuilders;

    public SettingMenuBuilder getSettingMenuByState(BotState botState) {
        return settingMenuBuilders.stream()
                                   .collect(Collectors.toMap(SettingMenuBuilder::getBotState, Function.identity()))
                                   .get(botState);
    }
}

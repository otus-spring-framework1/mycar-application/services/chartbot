package ru.otus.mycar.bot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.handler.UserAnswerHandler;
import ru.otus.mycar.bot.model.enums.BotState;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserAnswerHandlerFactory {
    private final List<UserAnswerHandler> userAnswerHandlers;

    public UserAnswerHandler getUserAnswerByBotState(BotState botState){
        return userAnswerHandlers.stream()
                                 .collect(Collectors.toMap(UserAnswerHandler::getHandlerName, Function.identity()))
                                 .get(botState);
    }
}

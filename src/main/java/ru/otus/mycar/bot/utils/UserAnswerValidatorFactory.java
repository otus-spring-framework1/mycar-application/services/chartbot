package ru.otus.mycar.bot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.validation.UserAnswerValidator;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserAnswerValidatorFactory {
    private final List<UserAnswerValidator> userAnswerValidators;

    public UserAnswerValidator getUserAnswerValidator(BotState botState) {
        return userAnswerValidators.stream()
                                   .collect(Collectors.toMap(UserAnswerValidator::getValidatorState, Function.identity()))
                                   .get(botState);
    }
}

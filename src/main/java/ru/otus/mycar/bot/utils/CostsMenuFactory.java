package ru.otus.mycar.bot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.costsMenuBuilder.CostsMenuStateBuilder;
import ru.otus.mycar.bot.model.enums.MetricType;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CostsMenuFactory {
    private final List<CostsMenuStateBuilder> costsMenuStateBuilders;

    public CostsMenuStateBuilder getCostsMenuByState(MetricType metricType) {
        return costsMenuStateBuilders.stream()
                                     .collect(Collectors.toMap(CostsMenuStateBuilder::getMetricName, Function.identity()))
                                     .get(metricType);
    }
}

package ru.otus.mycar.bot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.config.keyboardButtons.KeyboardButtonNames;
import ru.otus.mycar.bot.model.enums.KeyboardMenuButtons;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class KeyboardButtonsFactory {
    private final List<KeyboardButtonNames> keyboardButtonNames;

    public KeyboardButtonNames getButtons(KeyboardMenuButtons keyboardMenuButtons) {
        return keyboardButtonNames.stream()
                                     .collect(Collectors.toMap(KeyboardButtonNames::getButton, Function.identity()))
                                     .get(keyboardMenuButtons);
    }
}

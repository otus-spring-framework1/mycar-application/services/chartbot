package ru.otus.mycar.bot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.config.callBackButtons.CallBackButtonNames;
import ru.otus.mycar.bot.model.enums.CallBackMenu;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CallBackMenuFactory {
    private final List<CallBackButtonNames> callBackButtonNames;

    public CallBackButtonNames getButtons(CallBackMenu callBackMenu) {
        return callBackButtonNames.stream()
                                  .collect(Collectors.toMap(CallBackButtonNames::getCallBackMenuName, Function.identity()))
                                  .get(callBackMenu);
    }
}

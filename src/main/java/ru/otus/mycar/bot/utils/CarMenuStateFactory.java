package ru.otus.mycar.bot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.carMenuBuilder.CarMenuStateBuilder;
import ru.otus.mycar.bot.model.enums.BotState;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CarMenuStateFactory {
    private final List<CarMenuStateBuilder> carMenuStateBuilders;

    public CarMenuStateBuilder getCarMenuByState(BotState botState) {
        return carMenuStateBuilders.stream()
                      .collect(Collectors.toMap(CarMenuStateBuilder::getBotStateName, Function.identity()))
                      .get(botState);
    }
}

package ru.otus.mycar.bot.builder.settingsMenuBuilder.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.settingsMenuBuilder.SettingMenuBuilder;
import ru.otus.mycar.bot.model.CarMaintenance;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.model.enums.MaintenanceType;
import ru.otus.mycar.bot.request.CarMaintenanceRequestService;
import ru.otus.mycar.bot.storage.BotStateStorageService;

@Component
@RequiredArgsConstructor
public class ProcessScheduledMenuBuilder implements SettingMenuBuilder {
    private final CarMaintenanceRequestService requestService;
    private final BotStateStorageService botStateStorageService;

    @Override
    public void processUserAnswer(long userId, String userAnswer) {
        botStateStorageService.setCurrentBotState(userId, BotState.MEINTENANCE);
        requestService.sendCarMaintenance(new CarMaintenance().setUserId(String.valueOf(userId))
                                                              .setValue(Integer.parseInt(userAnswer))
                                                              .setMaintenanceType(MaintenanceType.SCHEDULE));
    }

    @Override
    public BotState getBotState() {
        return BotState.PROCESS_SCHEDULED;
    }
}

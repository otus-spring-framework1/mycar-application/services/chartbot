package ru.otus.mycar.bot.builder.statisticMenuBuilder;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.storage.BotStateStorageService;

@Component
@RequiredArgsConstructor
public class StatisticsMenuBuilder {
    private final BotStateStorageService botStateStorageService;

    public void setBotState(long userId){
        botStateStorageService.setCurrentBotState(userId, BotState.MY_STATISTICS);
    }
}

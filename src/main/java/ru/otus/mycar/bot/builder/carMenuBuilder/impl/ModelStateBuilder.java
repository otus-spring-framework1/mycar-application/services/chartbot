package ru.otus.mycar.bot.builder.carMenuBuilder.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.carMenuBuilder.CarMenuStateBuilder;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.storage.BotStateStorageService;

@Component
@RequiredArgsConstructor
public class ModelStateBuilder implements CarMenuStateBuilder {
    private final BotStateStorageService botStateStorageService;

    @Override
    public void processUserAnswer(long userId, String usersAnswer) {
        botStateStorageService.setCurrentBotState(userId, BotState.MODEL);
    }

    @Override
    public BotState getBotStateName(){
        return BotState.MODEL;
    }
}

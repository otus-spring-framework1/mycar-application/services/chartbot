package ru.otus.mycar.bot.builder.costsMenuBuilder.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.costsMenuBuilder.CostsMenuStateBuilder;
import ru.otus.mycar.bot.model.Metrics;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.model.enums.MetricType;
import ru.otus.mycar.bot.request.MetricsRequestService;
import ru.otus.mycar.bot.storage.BotStateStorageService;

import java.time.LocalDate;

@Component
@RequiredArgsConstructor
public class SparesCostsBuilder implements CostsMenuStateBuilder {
    private final BotStateStorageService botStateStorageService;
    private final MetricsRequestService metricsRequestService;

    @Override
    public void processUserAnswer(long userId, String usersAnswer) {
        botStateStorageService.setCurrentBotState(userId, BotState.MY_COSTS);
        metricsRequestService.sendMetricsToCarHandler(new Metrics().setUserId(String.valueOf(userId))
                                                                   .setMetricType(MetricType.SPARES)
                                                                   .setValue(usersAnswer)
                                                                   .setDate(LocalDate.now()));
    }

    @Override
    public MetricType getMetricName() {
        return MetricType.SPARES;
    }
}

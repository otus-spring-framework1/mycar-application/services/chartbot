package ru.otus.mycar.bot.builder.carMenuBuilder;

import ru.otus.mycar.bot.model.enums.BotState;

public interface CarMenuStateBuilder {
    void processUserAnswer(long userId, String usersAnswer);
    BotState getBotStateName();
}

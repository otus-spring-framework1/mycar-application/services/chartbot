package ru.otus.mycar.bot.builder.settingsMenuBuilder;

import ru.otus.mycar.bot.model.enums.BotState;

public interface SettingMenuBuilder {
    void processUserAnswer(long userId, String userAnswer);
    BotState getBotState();
}

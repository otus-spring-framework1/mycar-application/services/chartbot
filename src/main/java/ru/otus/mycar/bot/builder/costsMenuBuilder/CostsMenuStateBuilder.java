package ru.otus.mycar.bot.builder.costsMenuBuilder;

import ru.otus.mycar.bot.model.enums.MetricType;

public interface CostsMenuStateBuilder {
    void processUserAnswer(long userId, String usersAnswer);
    MetricType getMetricName();
}

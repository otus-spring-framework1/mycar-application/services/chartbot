package ru.otus.mycar.bot.builder.settingsMenuBuilder.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.settingsMenuBuilder.SettingMenuBuilder;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.storage.BotStateStorageService;

@Component
@RequiredArgsConstructor
public class ReminderMenuBuilder implements SettingMenuBuilder {
    private final BotStateStorageService botStateStorageService;

    @Override
    public void processUserAnswer(long userId, String userAnswer) {
        botStateStorageService.setCurrentBotState(userId, BotState.PROCESS_REMINDER);
    }

    @Override
    public BotState getBotState() {
        return BotState.REMIND;
    }
}

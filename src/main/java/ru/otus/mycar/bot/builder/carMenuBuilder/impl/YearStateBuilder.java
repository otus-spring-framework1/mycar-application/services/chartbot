package ru.otus.mycar.bot.builder.carMenuBuilder.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.carMenuBuilder.CarMenuStateBuilder;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.storage.BotStateStorageService;
import ru.otus.mycar.bot.storage.CarCacheService;

@Component
@RequiredArgsConstructor
public class YearStateBuilder implements CarMenuStateBuilder {
    private final BotStateStorageService botStateStorageService;
    private final CarCacheService carCacheService;

    @Override
    public void processUserAnswer(long userId, String usersAnswer) {
        botStateStorageService.setCurrentBotState(userId, BotState.YEAR);
        carCacheService.setCarTransmission(userId, usersAnswer);
    }

    @Override
    public BotState getBotStateName() {
        return BotState.YEAR;
    }
}

package ru.otus.mycar.bot.builder;

import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.model.dto.BotStateDto;
import ru.otus.mycar.bot.model.enums.BotState;

@Component
public class BotStateBuilder {

    public BotStateDto getBotStateWrapper(Long userId, BotState botState) {
        return new BotStateDto(String.valueOf(userId), botState);
    }
}

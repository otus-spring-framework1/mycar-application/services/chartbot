package ru.otus.mycar.bot.validation;

import ru.otus.mycar.bot.model.enums.BotState;

public interface UserAnswerValidator {
    void validateUserAnswer(String usersAnswer, String chatId);
    BotState getValidatorState();
}

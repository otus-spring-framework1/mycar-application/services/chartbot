package ru.otus.mycar.bot.validation.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.exceptions.UserAnswerValidationException;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.service.LanguageTranslatorService;
import ru.otus.mycar.bot.validation.UserAnswerValidator;

import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
public class ModelValidator implements UserAnswerValidator {
    private final LanguageTranslatorService translator;

    @Override
    public void validateUserAnswer(String usersAnswer, String chatId) {
        final Pattern pattern = Pattern.compile("\\w+");
        if (!pattern.matcher(usersAnswer).matches()) {
            throw new UserAnswerValidationException(chatId, translator.translateMessage("model.wrong.format"));
        }
    }

    @Override
    public BotState getValidatorState() {
        return BotState.MODEL;
    }
}

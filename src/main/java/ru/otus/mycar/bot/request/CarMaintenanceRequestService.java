package ru.otus.mycar.bot.request;

import ru.otus.mycar.bot.model.CarMaintenance;

import java.util.List;

public interface CarMaintenanceRequestService {
    void sendCarMaintenance(CarMaintenance carMaintenance);
    List<CarMaintenance> getCarMaintenance(String id);
}

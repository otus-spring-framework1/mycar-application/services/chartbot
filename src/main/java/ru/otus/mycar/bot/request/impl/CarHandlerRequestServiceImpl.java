package ru.otus.mycar.bot.request.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import ru.otus.mycar.bot.config.apiConfig.CarHandlerConfig;
import ru.otus.mycar.bot.model.Car;
import ru.otus.mycar.bot.request.CarHandlerRequestService;
import ru.otus.mycar.bot.service.RequestService;

@Service
@RequiredArgsConstructor
public class CarHandlerRequestServiceImpl implements CarHandlerRequestService {
    private final CarHandlerConfig carHandlerConfig;
    private final RequestService requestService;

    @Override
    public void sendCarToCarHandler(Car car) {
        requestService.post(car,carHandlerConfig.getCarSubPath());
    }

    @Override
    public Car getCarFromCarHandler(Long id) {
        ParameterizedTypeReference<Car> typeReference = new ParameterizedTypeReference<>() {};
        return requestService.get(carHandlerConfig.getCarSubPath() + "/" + id, typeReference);
    }
}

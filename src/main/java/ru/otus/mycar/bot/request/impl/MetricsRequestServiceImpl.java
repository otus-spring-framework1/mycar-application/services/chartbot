package ru.otus.mycar.bot.request.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.config.apiConfig.MetricsConfig;
import ru.otus.mycar.bot.model.Metrics;
import ru.otus.mycar.bot.model.enums.MetricType;
import ru.otus.mycar.bot.request.MetricsRequestService;
import ru.otus.mycar.bot.service.RequestService;

@Component
@RequiredArgsConstructor
public class MetricsRequestServiceImpl implements MetricsRequestService {
    private final RequestService requestService;
    private final MetricsConfig metricsConfig;

    @Override
    public void sendMetricsToCarHandler(Metrics metrics) {
        requestService.post(metrics, metricsConfig.getMetricSubPath());
    }

    @Override
    public Metrics getMetricsFromCarHandler(String userId, MetricType metricType) {
        ParameterizedTypeReference<Metrics> typeReference = new ParameterizedTypeReference<>() {};
        String uri = metricsConfig.getMetricSubPath() + "/" + userId + "/" + metricType;
        return requestService.get(uri, typeReference );
    }
}

package ru.otus.mycar.bot.request.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import ru.otus.mycar.bot.config.apiConfig.BotStateConfig;
import ru.otus.mycar.bot.model.dto.BotStateDto;
import ru.otus.mycar.bot.request.BotStateRequestService;
import ru.otus.mycar.bot.service.RequestService;

@Service
@RequiredArgsConstructor
public class BotStateRequestServiceImpl implements BotStateRequestService {
    private final BotStateConfig botStateConfig;
    private final RequestService requestService;

    @Override
    public void sendBotStateToCarHandler(BotStateDto botStateDto) {
        requestService.post(botStateDto,botStateConfig.getBoStateSubPath());
    }

    @Override
    public BotStateDto getBotStateFromCarHandler(Long id) {
        ParameterizedTypeReference<BotStateDto> typeReference = new ParameterizedTypeReference<>() {};
        return requestService.get(botStateConfig.getBoStateSubPath() + "/" + id, typeReference);
    }
}

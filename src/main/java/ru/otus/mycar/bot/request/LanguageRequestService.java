package ru.otus.mycar.bot.request;

public interface LanguageRequestService {
    void sendCurrentLanguage();
}

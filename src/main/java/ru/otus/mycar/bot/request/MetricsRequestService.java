package ru.otus.mycar.bot.request;

import ru.otus.mycar.bot.model.Metrics;
import ru.otus.mycar.bot.model.enums.MetricType;

public interface MetricsRequestService {
    void sendMetricsToCarHandler(Metrics metrics);

    Metrics getMetricsFromCarHandler(String userId, MetricType metricType);
}

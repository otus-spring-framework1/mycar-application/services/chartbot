package ru.otus.mycar.bot.request;

import ru.otus.mycar.bot.model.Car;

public interface CarHandlerRequestService {
    void sendCarToCarHandler(Car car);
    Car getCarFromCarHandler(Long id);
}

package ru.otus.mycar.bot.request.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import ru.otus.mycar.bot.config.apiConfig.CarMaintenanceConfig;
import ru.otus.mycar.bot.model.CarMaintenance;
import ru.otus.mycar.bot.request.CarMaintenanceRequestService;
import ru.otus.mycar.bot.service.RequestService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarMaintenanceRequestServiceImpl implements CarMaintenanceRequestService {
    private final RequestService requestService;
    private final CarMaintenanceConfig config;

    @Override
    public void sendCarMaintenance(CarMaintenance carMaintenance){
        requestService.post(carMaintenance,config.getMaintenanceSubPath());
    }

    @Override
    public List<CarMaintenance> getCarMaintenance(String id){
        ParameterizedTypeReference<List<CarMaintenance>> typeReference = new ParameterizedTypeReference<>() {};
        return requestService.get(config.getMaintenanceSubPath() + "/" + id, typeReference);
    }
}

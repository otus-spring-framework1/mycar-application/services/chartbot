package ru.otus.mycar.bot.request;

import ru.otus.mycar.bot.model.dto.BotStateDto;

public interface BotStateRequestService {
    void sendBotStateToCarHandler(BotStateDto botStateDto);
    BotStateDto getBotStateFromCarHandler(Long id);
}

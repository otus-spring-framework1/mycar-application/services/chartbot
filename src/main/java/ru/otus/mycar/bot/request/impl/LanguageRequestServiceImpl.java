package ru.otus.mycar.bot.request.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.otus.mycar.bot.config.LanguageProperty;
import ru.otus.mycar.bot.config.apiConfig.LocaleConfig;
import ru.otus.mycar.bot.request.LanguageRequestService;
import ru.otus.mycar.bot.service.RequestService;

@Service
@RequiredArgsConstructor
public class LanguageRequestServiceImpl implements LanguageRequestService {
    private final RequestService requestService;
    private final LanguageProperty languageProperty;
    private final LocaleConfig config;

    @Override
    public void sendCurrentLanguage(){
        requestService.post(languageProperty.getCurrentLanguage(),config.getLocalSubPath());
    }
}

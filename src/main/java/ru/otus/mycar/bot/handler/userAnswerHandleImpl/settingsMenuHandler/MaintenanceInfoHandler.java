package ru.otus.mycar.bot.handler.userAnswerHandleImpl.settingsMenuHandler;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.otus.mycar.bot.builder.MessageBuilder;
import ru.otus.mycar.bot.handler.UserAnswerHandler;
import ru.otus.mycar.bot.model.CarMaintenance;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.model.enums.MaintenanceType;
import ru.otus.mycar.bot.request.CarMaintenanceRequestService;
import ru.otus.mycar.bot.utils.SettingMenuFactory;

import java.util.List;

@Component
@RequiredArgsConstructor
public class MaintenanceInfoHandler implements UserAnswerHandler {
    private final MessageBuilder messageBuilder;
    private final SettingMenuFactory settingMenuFactory;
    private final CarMaintenanceRequestService carMaintenanceRequestService;

    @Override
    public SendMessage handleUserAnswer(String usersAnswer, long userId, String chatId) {
        settingMenuFactory.getSettingMenuByState(BotState.MAINTENANCE_INFO).processUserAnswer(userId, usersAnswer);
        return SendMessage.builder()
                          .chatId(chatId)
                          .text(getMessage(userId))
                          .build();
    }

    @Override
    public BotState getHandlerName() {
        return BotState.MAINTENANCE_INFO;
    }

    private String getMessage(Long userId) {
        List<CarMaintenance> carMaintenanceList = carMaintenanceRequestService.getCarMaintenance(String.valueOf(userId));
        return messageBuilder.getTranslatedMessage("total.message.output") + ": " + getCarMaintenanceByType(carMaintenanceList, MaintenanceType.TOTAL) + "\n" +
                messageBuilder.getTranslatedMessage("reminder.message.output") + ": " + getCarMaintenanceByType(carMaintenanceList, MaintenanceType.REMIND) + "\n" +
                messageBuilder.getTranslatedMessage("schedule.message.output") + ": " + getCarMaintenanceByType(carMaintenanceList, MaintenanceType.SCHEDULE);
    }

    private int getCarMaintenanceByType(List<CarMaintenance> carMaintenanceList, MaintenanceType type) {
        return carMaintenanceList.stream()
                                 .filter(carMaintenance -> carMaintenance.getMaintenanceType() == type)
                                 .map(CarMaintenance::getValue)
                                 .findFirst()
                                 .orElse(0);
    }
}

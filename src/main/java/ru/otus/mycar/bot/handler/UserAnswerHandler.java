package ru.otus.mycar.bot.handler;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.otus.mycar.bot.model.enums.BotState;

public interface UserAnswerHandler {
    SendMessage handleUserAnswer(String usersAnswer, long userId, String chatId);
    BotState getHandlerName();
}

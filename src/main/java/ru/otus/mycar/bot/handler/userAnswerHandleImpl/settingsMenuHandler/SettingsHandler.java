package ru.otus.mycar.bot.handler.userAnswerHandleImpl.settingsMenuHandler;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.otus.mycar.bot.builder.MessageBuilder;
import ru.otus.mycar.bot.handler.UserAnswerHandler;
import ru.otus.mycar.bot.model.enums.BotApiNames;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.utils.BotApiFactory;

@Component
@RequiredArgsConstructor
public class SettingsHandler implements UserAnswerHandler {
    private final MessageBuilder messageBuilder;
    private final BotApiFactory botApiFactory;

    @Override
    public SendMessage handleUserAnswer(String usersAnswer, long userId, String chatId) {
        return messageBuilder.createMessageWithKeyboard(Long.parseLong(chatId), messageBuilder.getTranslatedMessage("setting.data"),
                botApiFactory.getBotApi(BotApiNames.SETTINGS_MENU_API).getApi());
    }

    @Override
    public BotState getHandlerName() {
        return BotState.SETTINGS;
    }
}

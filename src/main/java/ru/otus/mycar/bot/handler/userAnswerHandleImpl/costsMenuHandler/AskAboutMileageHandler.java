package ru.otus.mycar.bot.handler.userAnswerHandleImpl.costsMenuHandler;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.otus.mycar.bot.builder.MessageBuilder;
import ru.otus.mycar.bot.handler.UserAnswerHandler;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.storage.BotStateStorageService;

@Component
@RequiredArgsConstructor
public class AskAboutMileageHandler implements UserAnswerHandler {
    private final MessageBuilder messageBuilder;
    private final BotStateStorageService botStateStorageService;

    @Override
    public SendMessage handleUserAnswer(String usersAnswer, long userId, String chatId) {
        botStateStorageService.setCurrentBotState(userId, BotState.COSTS_MILEAGE);
        return SendMessage.builder()
                          .chatId(chatId)
                          .text(messageBuilder.getTranslatedMessage("costs.mileage"))
                          .build();
    }

    @Override
    public BotState getHandlerName() {
        return BotState.ASK_ABOUT_MILEAGE_COSTS;
    }
}

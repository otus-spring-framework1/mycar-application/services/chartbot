package ru.otus.mycar.bot.handler.userAnswerHandleImpl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.otus.mycar.bot.builder.MessageBuilder;
import ru.otus.mycar.bot.handler.UserAnswerHandler;
import ru.otus.mycar.bot.model.enums.BotApiNames;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.utils.BotApiFactory;

@Component
@RequiredArgsConstructor
public class MainMenuHandler implements UserAnswerHandler {
    private final MessageBuilder messageBuilder;
    private final BotApiFactory botApiFactory;

    @Override
    public BotState getHandlerName() {
        return BotState.INFORM;
    }

    @Override
    public SendMessage handleUserAnswer(String usersAnswer, long userId, String chatId) {
        return messageBuilder.createMessageWithKeyboard(Long.parseLong(chatId), messageBuilder.getTranslatedMessage("car.main.menu"),
                botApiFactory.getBotApi(BotApiNames.MAIN_MENU_API).getApi());
    }
}

package ru.otus.mycar.bot.handler.userAnswerHandleImpl.carMenuHandler;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.otus.mycar.bot.builder.MessageBuilder;
import ru.otus.mycar.bot.handler.UserAnswerHandler;
import ru.otus.mycar.bot.model.Car;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.storage.CarCacheService;
import ru.otus.mycar.bot.utils.CarMenuStateFactory;

@Component
@RequiredArgsConstructor
public class RegisterNewCar implements UserAnswerHandler {
    private final MessageBuilder messageBuilder;
    private final CarMenuStateFactory carMenuStateFactory;
    private final CarCacheService carCacheService;

    @Override
    public SendMessage handleUserAnswer(String usersAnswer, long userId, String chatId) {
        carMenuStateFactory.getCarMenuByState(BotState.MODEL)
                           .processUserAnswer(userId, usersAnswer);
        carCacheService.saveCarInCache(userId, new Car().setId(String.valueOf(userId))
                                                        .setChatId(chatId));

        return SendMessage.builder()
                          .chatId(chatId)
                          .text(messageBuilder.getTranslatedMessage("car.model"))
                          .build();
    }

    @Override
    public BotState getHandlerName() {
        return BotState.REGISTER;
    }
}

package ru.otus.mycar.bot.handler.userAnswerHandleImpl.costsMenuHandler;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.otus.mycar.bot.builder.MessageBuilder;
import ru.otus.mycar.bot.handler.UserAnswerHandler;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.model.enums.MetricType;
import ru.otus.mycar.bot.utils.CostsMenuFactory;
import ru.otus.mycar.bot.utils.UserAnswerValidatorFactory;

@Component
@RequiredArgsConstructor
public class SparesHandler implements UserAnswerHandler {
    private final MessageBuilder messageBuilder;
    private final CostsMenuFactory costsMenuFactory;
    private final UserAnswerValidatorFactory answerValidator;

    @Override
    public SendMessage handleUserAnswer(String usersAnswer, long userId, String chatId) {
        answerValidator.getUserAnswerValidator(BotState.METRICS).validateUserAnswer(usersAnswer, chatId);
        costsMenuFactory.getCostsMenuByState(MetricType.SPARES).processUserAnswer(userId,usersAnswer);
        return SendMessage.builder()
                          .chatId(chatId)
                          .text(messageBuilder.getTranslatedMessage("costs.choose.menu"))
                          .build();
    }

    @Override
    public BotState getHandlerName() {
        return BotState.COSTS_SPARES;
    }
}

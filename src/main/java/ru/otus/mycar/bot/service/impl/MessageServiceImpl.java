package ru.otus.mycar.bot.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.service.MessageService;
import ru.otus.mycar.bot.service.ReplyButtonNamesService;
import ru.otus.mycar.bot.storage.BotStateStorageService;
import ru.otus.mycar.bot.utils.UserAnswerHandlerFactory;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {
    private final BotStateStorageService botStateStorageService;
    private final UserAnswerHandlerFactory userAnswerFactory;
    private final ReplyButtonNamesService replyButtonNamesService;

    @Override
    public BotApiMethod<?> handleMessage(Message message) {
        final String inputMessage = message.getText();
        final long userId = message.getFrom().getId();

        BotState botState = getBotState(inputMessage, userId);
        botStateStorageService.setCurrentBotState(userId, botState);

//        return SendMessage.builder().chatId(String.valueOf(message.getChatId())).text("hello world").build();
        return replayMessage(botState, message);
    }

    private BotState getBotState(String inputMessage, long userId) {
        return replyButtonNamesService.getBotStateByMessage(inputMessage, userId);
    }

    private BotApiMethod<?> replayMessage(BotState botState, Message message) {
        final String usersAnswer = message.getText();
        final long userId = message.getFrom().getId();
        final String chatId = message.getChatId().toString();

        return userAnswerFactory.getUserAnswerByBotState(botState)
                                .handleUserAnswer(usersAnswer, userId, chatId);
    }
}

package ru.otus.mycar.bot.service;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

public interface CallbackQueryService {
    BotApiMethod<?> handleCallbackQuery(CallbackQuery callbackQuery);
}

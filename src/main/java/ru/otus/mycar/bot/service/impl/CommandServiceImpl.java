package ru.otus.mycar.bot.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import ru.otus.mycar.bot.builder.MessageBuilder;
import ru.otus.mycar.bot.model.enums.BotApiNames;
import ru.otus.mycar.bot.service.CommandService;
import ru.otus.mycar.bot.utils.BotApiFactory;


@Service
@RequiredArgsConstructor
public class CommandServiceImpl implements CommandService {
    private final BotApiFactory botApiFactory;
    private final MessageBuilder messageBuilder;

    @Override
    public BotApiMethod<?> handleCommand(Message message) {
        if (message.getText().equals("/start")) {
            return messageBuilder.createMessageWithKeyboard(message.getChatId(),
                    messageBuilder.getTranslatedMessage("car.main.menu"),
                    botApiFactory.getBotApi(BotApiNames.MAIN_MENU_API).getApi());
        }
        return SendMessage.builder()
                          .text("Wrong command. Try again")
                          .chatId(String.valueOf(message.getChatId()))
                          .build();
    }
}


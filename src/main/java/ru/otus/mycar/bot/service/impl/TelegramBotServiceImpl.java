package ru.otus.mycar.bot.service.impl;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.starter.SpringWebhookBot;
import ru.otus.mycar.bot.config.TelegramBotConfig;
import ru.otus.mycar.bot.service.TelegramBotService;
import ru.otus.mycar.bot.service.UpdateService;


@Service
public class TelegramBotServiceImpl extends SpringWebhookBot implements TelegramBotService {

    private final UpdateService updateService;
    private final TelegramBotConfig botConfig;

    public TelegramBotServiceImpl(UpdateService updateService, SetWebhook setWebhook, TelegramBotConfig botConfig) {
        super(setWebhook);
        this.updateService = updateService;
        this.botConfig = botConfig;
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {
        return updateService.processUpdate(update);
    }

    @Override
    public String getBotPath() {
        return botConfig.getPath();
    }

    @Override
    public String getBotUsername() {
        return botConfig.getUsername();
    }

    @Override
    public String getBotToken() {
        return botConfig.getToken();
    }
}

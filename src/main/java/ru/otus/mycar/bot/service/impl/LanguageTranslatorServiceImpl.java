package ru.otus.mycar.bot.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.otus.mycar.bot.config.LanguageProperty;
import ru.otus.mycar.bot.service.LanguageTranslatorService;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class LanguageTranslatorServiceImpl implements LanguageTranslatorService {

    private final MessageSource messageSource;
    private final LanguageProperty languageProperty;

    @Override
    public String translateMessage(String message) {
        Locale locale = new Locale(languageProperty.getCurrentLanguage(), languageProperty.getCurrentLanguage().toUpperCase());
        return messageSource.getMessage(message, new Object[0], locale);
    }

}

package ru.otus.mycar.bot.service.impl;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.otus.mycar.bot.exceptions.UserAnswerValidationException;
import ru.otus.mycar.bot.service.CallbackQueryService;
import ru.otus.mycar.bot.service.CommandService;
import ru.otus.mycar.bot.service.MessageService;
import ru.otus.mycar.bot.service.UpdateService;


@Service
@RequiredArgsConstructor
public class UpdateServiceImpl implements UpdateService {

    private final MessageService messageService;
    private final CommandService commandService;
    private final CallbackQueryService callbackQueryService;

    @Override
    public BotApiMethod<?> processUpdate(Update update) {
        try {
            if (update.hasMessage() && update.getMessage().isCommand()) {
                return commandService.handleCommand(update.getMessage());
            }
            if (update.hasMessage() && update.getMessage().hasText()) {
                return messageService.handleMessage(update.getMessage());
            }
            if (update.hasCallbackQuery()) {
                return callbackQueryService.handleCallbackQuery(update.getCallbackQuery());
            }
        } catch (UserAnswerValidationException ex) {
            return SendMessage.builder()
                              .chatId(ex.getChartId())
                              .text(ex.getMessage())
                              .build();
        }
        return null;
    }
}

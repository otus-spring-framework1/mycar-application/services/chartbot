package ru.otus.mycar.bot.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.otus.mycar.bot.config.ReplyButtonNamesConfig;
import ru.otus.mycar.bot.model.KeyboardButtonName;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.service.ReplyButtonNamesService;
import ru.otus.mycar.bot.storage.BotStateStorageService;

@Service
@RequiredArgsConstructor
public class ReplyButtonNamesServiceImpl implements ReplyButtonNamesService {
    private final ReplyButtonNamesConfig config;
    private final BotStateStorageService botStateStorageService;

    @Override
    public BotState getBotStateByMessage(String inputMessage, long userId) {
        return config.getButtonNames()
                     .stream()
                     .filter(button -> button.getName().equals(inputMessage))
                     .map(KeyboardButtonName::getBotState)
                     .findFirst()
                     .orElse(botStateStorageService.getUsersCurrentBotState(userId).getBotState());
    }
}

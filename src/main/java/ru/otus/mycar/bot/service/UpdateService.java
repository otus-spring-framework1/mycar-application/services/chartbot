package ru.otus.mycar.bot.service;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface UpdateService {
    BotApiMethod<?> processUpdate(Update update);
}

package ru.otus.mycar.bot.service;

import org.springframework.core.ParameterizedTypeReference;

public interface RequestService {
    <T> T get(String uri, ParameterizedTypeReference<T> typeReference);
    <T> void post(T body, String uri);
}

package ru.otus.mycar.bot.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import ru.otus.mycar.bot.config.callBackButtons.CallBackButtonNames;
import ru.otus.mycar.bot.model.ButtonSateWrapper;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.service.CallbackQueryService;
import ru.otus.mycar.bot.storage.BotStateStorageService;
import ru.otus.mycar.bot.utils.UserAnswerHandlerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CallbackQueryServiceImpl implements CallbackQueryService {
    private final BotStateStorageService botStateStorageService;
    private final UserAnswerHandlerFactory userAnswerFactory;
    private final List<CallBackButtonNames> callBackButtonNames;

    @Override
    public BotApiMethod<?> handleCallbackQuery(CallbackQuery callbackQuery) {
        long userId = callbackQuery.getFrom().getId();
        final BotState botState = getBotState(callbackQuery.getData(), userId);

//        return SendMessage.builder().chatId(String.valueOf(chatId)).text("hello world").build();
        return replayMessage(botState, callbackQuery);
    }

    private BotState getBotState(String calBackData, long userId) {
        return getMapWithBotStateAndCallBackData().entrySet()
                                                  .stream()
                                                  .filter(entry -> entry.getValue().contains(calBackData))
                                                  .map(Map.Entry::getKey)
                                                  .findFirst()
                                                  .orElse(botStateStorageService.getUsersCurrentBotState(userId)
                                                                                .getBotState());
    }

    private Map<BotState, Set<String>> getMapWithBotStateAndCallBackData() {
        return callBackButtonNames.stream()
                                  .map(CallBackButtonNames::getButtonWrapper)
                                  .collect(Collectors.toMap(ButtonSateWrapper::getBotState,
                                        buttonSateWrapper -> buttonSateWrapper.getButtonNames()
                                                                              .keySet()
                          ));
    }

    private BotApiMethod<?> replayMessage(BotState botState, CallbackQuery callbackQuery) {
        final long userId = callbackQuery.getFrom().getId();
        final String chatId = callbackQuery.getMessage().getChatId().toString();
        String data = callbackQuery.getData();

        return userAnswerFactory.getUserAnswerByBotState(botState)
                                .handleUserAnswer(data, userId, chatId);
    }
}

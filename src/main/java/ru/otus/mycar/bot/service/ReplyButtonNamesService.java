package ru.otus.mycar.bot.service;

import ru.otus.mycar.bot.model.enums.BotState;

public interface ReplyButtonNamesService {
    BotState getBotStateByMessage(String inputMessage, long userId);
}

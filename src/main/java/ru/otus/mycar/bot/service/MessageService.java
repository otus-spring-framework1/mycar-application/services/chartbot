package ru.otus.mycar.bot.service;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface MessageService {
    BotApiMethod<?> handleMessage(Message message);
}

package ru.otus.mycar.bot.service;


public interface LanguageTranslatorService {
    String translateMessage(String message);
}

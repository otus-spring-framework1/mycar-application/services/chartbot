package ru.otus.mycar.bot.config.keyboardButtons;

import ru.otus.mycar.bot.model.KeyboardButtonName;
import ru.otus.mycar.bot.model.enums.KeyboardMenuButtons;

import java.util.List;

public interface KeyboardButtonNames {
    List<KeyboardButtonName> getButtonNames();
    KeyboardMenuButtons getButton();
}

package ru.otus.mycar.bot.config.callBackButtons;

import ru.otus.mycar.bot.model.ButtonSateWrapper;
import ru.otus.mycar.bot.model.CallBackButtons;
import ru.otus.mycar.bot.model.enums.CallBackMenu;

import java.util.List;

public interface CallBackButtonNames {
    List<CallBackButtons> getButtonNames();
    ButtonSateWrapper getButtonWrapper();
    CallBackMenu getCallBackMenuName();

}

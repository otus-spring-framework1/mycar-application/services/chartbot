package ru.otus.mycar.bot.config.callBackButtons.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.otus.mycar.bot.builder.MessageBuilder;
import ru.otus.mycar.bot.config.callBackButtons.CallBackButtonNames;
import ru.otus.mycar.bot.model.ButtonSateWrapper;
import ru.otus.mycar.bot.model.CallBackButtons;
import ru.otus.mycar.bot.model.enums.BotState;
import ru.otus.mycar.bot.model.enums.CallBackMenu;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class LanguageCallBackButtonNames implements CallBackButtonNames {
    private final MessageBuilder messageBuilder;
    private final Map<String, String> buttonNames;

    public LanguageCallBackButtonNames(@Value("#{${buttons.callback.language}}") Map<String, String> buttonNames, MessageBuilder messageBuilder) {
        this.messageBuilder = messageBuilder;
        this.buttonNames = buttonNames;
    }

    @Override
    public List<CallBackButtons> getButtonNames() {
        return buttonNames.entrySet()
                          .stream()
                          .map(entry -> new CallBackButtons().setCallBackKey(entry.getKey())
                                                             .setButtonName(messageBuilder.getTranslatedMessage(entry.getValue())))
                          .collect(Collectors.toList());
    }

    @Override
    public ButtonSateWrapper getButtonWrapper() {
        return new ButtonSateWrapper().setButtonNames(buttonNames)
                                      .setBotState(BotState.PROCESS_LANGUAGE);
    }

    @Override
    public CallBackMenu getCallBackMenuName() {
        return CallBackMenu.LANGUAGE_MENU;
    }
}
